import { DateTime } from 'luxon'

export interface SeasonInfo {
	series : SeriesInfo,
	sim : SimulatorInfo,
}

export interface SimulatorInfo {
	code : string,
	name : string,
}

export interface SeriesInfo {
	code : string,
	name : string,
}

export interface TrackInfo {
	code : string,
	name : string,
}

export interface DriverLapResult {
	lapNumber : number,
	position : number,
	lapTime : number,
	s1Time : number,
	s2Time : number,
	s3Time : number,
}

export interface DriverRaceResult {
	position : number,
	country : string,
	name : string,
	carName : string,
	carBrand? : string,
	bestLapTime : number,
	bestS1 : number,
	bestS2 : number,
	bestS3 : number,
	finishTime : number,
	lapsNumber : number,
	laps : Array<DriverLapResult>,
	dnf : boolean,
	incidents : number,
	points : number,
	totalPoints : number,
}

export interface SeasonResults {
	seasonInfo: SeasonInfo,
	raceResultsList: Array<RaceResult>,
}

export interface RaceResult {
	dateTime : DateTime | string,
	split : number,
	track : TrackInfo,
	drivers : Array<DriverRaceResult>,
}

export interface FastestLapLeaderboardResult {
	position : number,
	driverName : string,
	country : string,
	lapTime : number,
}

export interface EventFastestLapLeaderboards {
	event : TrackInfo,
	fastestLaps: Array<FastestLapLeaderboardResult>,
}

export interface SeasonFastestLapLeaderboards {
	seasonInfo : SeasonInfo,
	qualifyingFastestLaps: Array<EventFastestLapLeaderboards>,
	raceFastestLaps: Array<EventFastestLapLeaderboards>,
}
