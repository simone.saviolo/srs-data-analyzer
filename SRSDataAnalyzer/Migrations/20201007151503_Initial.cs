﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SRSDataAnalyzer.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RaceProgressions",
                columns: table => new
                {
                    ID = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RaceProgressions", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DriverRaceResult",
                columns: table => new
                {
                    ID = table.Column<string>(nullable: false),
                    Position = table.Column<int>(nullable: false),
                    DriverName = table.Column<string>(nullable: true),
                    DriverNationality = table.Column<string>(nullable: true),
                    CarName = table.Column<string>(nullable: true),
                    Incidents = table.Column<int>(nullable: false),
                    Points = table.Column<int>(nullable: false),
                    RaceProgressionID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverRaceResult", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DriverRaceResult_RaceProgressions_RaceProgressionID",
                        column: x => x.RaceProgressionID,
                        principalTable: "RaceProgressions",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DriverLapResult",
                columns: table => new
                {
                    ID = table.Column<string>(nullable: false),
                    LapNumber = table.Column<int>(nullable: false),
                    LapTime = table.Column<float>(nullable: false),
                    Sector1Time = table.Column<float>(nullable: false),
                    Sector2Time = table.Column<float>(nullable: false),
                    Sector3Time = table.Column<float>(nullable: false),
                    Position = table.Column<int>(nullable: false),
                    DriverRaceResultID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverLapResult", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DriverLapResult_DriverRaceResult_DriverRaceResultID",
                        column: x => x.DriverRaceResultID,
                        principalTable: "DriverRaceResult",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DriverLapResult_DriverRaceResultID",
                table: "DriverLapResult",
                column: "DriverRaceResultID");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRaceResult_RaceProgressionID",
                table: "DriverRaceResult",
                column: "RaceProgressionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DriverLapResult");

            migrationBuilder.DropTable(
                name: "DriverRaceResult");

            migrationBuilder.DropTable(
                name: "RaceProgressions");
        }
    }
}
