﻿using SRSDataAnalyzer.Data;

namespace SRSDataAnalyzer.Models.RaceProgression
{
	public class RaceProgressionModel
	{
		public SRSDataAnalyzer.Data.RaceProgression Data { get; set; } = new SRSDataAnalyzer.Data.RaceProgression();
	}
}
