﻿using System.Collections.Generic;
using System.Linq;

namespace SRSDataAnalyzer.Data
{
	public class DriverRaceResult
	{
		public string ID { get; set; }

		public int Position { get; set; } = 0;
		public string DriverName { get; set; } = "-- No Name --";
		public string DriverNationality { get; set; } = "";
		public string CarName { get; set; } = "-- Unknown Car --";
		public int Incidents { get; set; } = 0;
		public int Points { get; set; } = 0;
		public List<DriverLapResult> LapResults { get; set; } = new List<DriverLapResult>();

		public int TotalPoints => Points - Incidents * 3;
		public DriverLapResult BestLap => LapResults.OrderBy(lr => lr.LapTime).FirstOrDefault();

		public string RaceProgressionID { get; set; }
		public RaceProgression RaceProgression { get; set; }

		public DriverRaceResult()
		{
			ID = DataUtilities.GenerateRandomString();
		}
	}
}
