﻿namespace SRSDataAnalyzer.Data
{
	public class DriverLapResult
	{
		public string ID { get; set; }
		public int LapNumber { get; set; } = 0;
		public float LapTime { get; set; } = 0.0f;
		public float Sector1Time { get; set; } = 0.0f;
		public float Sector2Time { get; set; } = 0.0f;
		public float Sector3Time { get; set; } = 0.0f;
		public int Position { get; set; } = 0;

		public string DriverRaceResultID { get; set; }
		public DriverRaceResult DriverRaceResult { get; set; }

		public DriverLapResult()
		{
			ID = DataUtilities.GenerateRandomString();
		}
	}
}