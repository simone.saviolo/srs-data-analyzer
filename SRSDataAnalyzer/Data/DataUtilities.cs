﻿using System;

namespace SRSDataAnalyzer.Data
{
	public static class DataUtilities
	{
		public static string GenerateRandomString()
		{
			return Guid.NewGuid().ToString();
		}
	}
}
