﻿using Microsoft.EntityFrameworkCore;

namespace SRSDataAnalyzer.Data
{
	public class DataContext : DbContext
	{
		public DbSet<RaceProgression> RaceProgressions { get; set; }
		public DbSet<DriverRaceResult> DriverRaceResult { get; set; }
		public DbSet<DriverLapResult> DriverLapResult { get; set; }

		public DataContext(DbContextOptions<DataContext> i_contextOptions)
			: base(i_contextOptions)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<RaceProgression>(entity => 
			{
				entity.HasKey(e => e.ID);
				entity.HasMany(e => e.DriverRaceResults).WithOne(drr => drr.RaceProgression).HasForeignKey(drr => drr.RaceProgressionID).OnDelete(DeleteBehavior.Cascade);
			});

			modelBuilder.Entity<DriverRaceResult>(entity => 
			{
				entity.HasKey(e => e.ID);
				entity.HasMany(e => e.LapResults).WithOne(dlr => dlr.DriverRaceResult).HasForeignKey(dlr => dlr.DriverRaceResultID).OnDelete(DeleteBehavior.Cascade);
			});

			modelBuilder.Entity<DriverLapResult>(entity => 
			{
				entity.HasKey(e => e.ID);
			});
		}
	}
}
