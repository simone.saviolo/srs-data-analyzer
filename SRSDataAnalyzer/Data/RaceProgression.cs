﻿using System.Collections.Generic;

namespace SRSDataAnalyzer.Data
{
	public class RaceProgression
	{
		public string ID { get; set; }
		public List<DriverRaceResult> DriverRaceResults { get; set; } = new List<DriverRaceResult>();

		public RaceProgression()
		{
			ID = DataUtilities.GenerateRandomString();
		}
	}
}
