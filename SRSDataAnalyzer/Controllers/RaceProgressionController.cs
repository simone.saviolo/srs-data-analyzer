﻿using Microsoft.AspNetCore.Mvc;
using SRSDataAnalyzer.Data;
using SRSDataAnalyzer.Models.RaceProgression;
using System.Linq;
using System.Threading.Tasks;

namespace SRSDataAnalyzer.Controllers
{
	public class RaceProgressionController : Controller
	{
		private readonly DataContext m_dataContext = null;

		public RaceProgressionController(DataContext i_dataContext)
		{
			m_dataContext = i_dataContext;
		}

		[HttpGet]
		public IActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> AnalyzeRaceFromText(Index formInput)
		{
			if (formInput.RaceResultsText != null)
			{
				RaceProgression raceProgression = new RaceProgression();

				raceProgression.DriverRaceResults.Add(new DriverRaceResult() { Position = 1, DriverName = "Test 1", Points = 100, Incidents = 10 });
				raceProgression.DriverRaceResults.Add(new DriverRaceResult() { Position = 2, DriverName = "Test 2", Points = 90, Incidents = 1 });

				m_dataContext.Add(raceProgression);
				await m_dataContext.SaveChangesAsync();

				return RedirectToAction(nameof(ShowRaceProgression), new { id = raceProgression.ID });
			}

			return BadRequest();
		}

		[HttpGet]
		public IActionResult ShowRaceProgression(string id)
		{
			RaceProgressionModel model = new RaceProgressionModel()
			{
				Data = m_dataContext.RaceProgressions.SingleOrDefault(rp => rp.ID == id),
			};
			return View("RaceAnalysis", model);
		}
	}
}
