const data = {
	seasonInfo: null,
	qualifyingFastestLaps: null,
	raceFastestLaps: null,
}

const actOnSeriesInfoTable = (i_table) => {
	const cell = i_table.querySelector('tbody tr:first-child > td')
	if (!cell) return;

	const imgs = cell.querySelectorAll('img')
	const seriesCode = /serieslogo\/(?<seriesCode>.+)\.png/.exec(imgs.item(0).getAttribute('src')).groups.seriesCode
	const simCode = /results\/(?<simCode>.+)\.png/.exec(imgs.item(1).getAttribute('src')).groups.simCode
	
	return {
		series: {
			code: seriesCode,
			name: seriesCode,
		},
		sim: {
			code: simCode,
			name: simCode,
		},
	}
}

const actOnFastestLapsTable = (i_table) => {
	const retval = []
	i_table.querySelectorAll('tbody > tr').forEach(eventRow => {
		const eventLeaderboard = {
			event: null,
			fastestLaps: [],
		}
		eventRow.querySelectorAll('td').forEach((cell, i) => {
			if (i === 0) {
				const img = cell.querySelector('img')
				const trackCode = /tracklogo\/45\/(?<trackCode>.+).png/.exec(img.getAttribute('src')).groups.trackCode
				eventLeaderboard.event = { code: trackCode }
			}
			else {
				if (cell.textContent === ' - ') {
					return
				}
				const img = cell.querySelector('img')
				const country = /images\/flags\/(?<country>.+).png/.exec(img.getAttribute('src')).groups.country
				const textParsing = / (?<name>[a-zA-Z ]+) (?<time>\d{2}:\d{2}.\d{0,3})/.exec(cell.textContent)
				const fastestLap = {
					position: i,
					driverName: textParsing.groups.name,
					country: country,
					lapTime: parseSRSTime(textParsing.groups.time),
				}
				eventLeaderboard.fastestLaps.push(fastestLap)
			}
		})
		retval.push(eventLeaderboard)
	})
	return retval
}

document.querySelectorAll('table').forEach(table => {
	if (table.classList.contains('tborder')) {
		const parent = table.parentElement
		if (table.querySelectorAll('thead').length === 0) {
			data.seasonInfo = actOnSeriesInfoTable(table)
		}
		else {
			if (!data.qualifyingFastestLaps) {
				data.qualifyingFastestLaps = actOnFastestLapsTable(table)
			}
			else {
				data.raceFastestLaps = actOnFastestLapsTable(table)
			}
		}
	}
})

sendMessage(ContentScriptMessageTypes.pageAnalysisCompleted, {
	pageType: PageTypes.fastestLaps,
	seriesInfo: data.seasonInfo,
	qualifyingFastestLaps: data.qualifyingFastestLaps,
	raceFastestLaps: data.raceFastestLaps,
})

