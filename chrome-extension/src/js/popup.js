function setPageType(i_pageType) {
	document.getElementById('pageType').innerText = i_pageType || 'Page not recognized'
}
function setErrorMessage(i_errorMessage) {
	document.getElementById('errorMessage').innerText = i_errorMessage
}
function clearErrorMessage() {
	document.getElementById('errorMessage').innerText = ''
}

function handlePageAnalysisResults(i_pageType, i_pageAnalysis) {
	if (!i_pageAnalysis) {
		setPageType('Analysis unavailable')
		setErrorMessage('If you think the page can be analyzed, please reload the page to run the analysis again.')
		return
	}
	const pageContents = document.getElementById('pageContents')
	switch(i_pageAnalysis.pageType)
	{
		case PageTypes.raceResults: 
			setPageType('Race results')
			clearErrorMessage()
			removeAllChildren(pageContents)
			pageContents.appendChild(raceResultsContents(i_pageAnalysis.raceResults))
			break
		case PageTypes.raceResultsList: 
			setPageType('Season races results')
			clearErrorMessage()
			removeAllChildren(pageContents)
			pageContents.appendChild(seasonResultsContents({ 
				seasonInfo: i_pageAnalysis.seriesInfo,
				racesList: i_pageAnalysis.racesList.map(r => ({ ...r, dateTime: luxon.DateTime.fromISO(r.dateTime)})),
			}))
			break
		case PageTypes.fastestLaps:
			setPageType('Fastest laps leaderboard')
			clearErrorMessage()
			removeAllChildren(pageContents)
			pageContents.appendChild(fastestLapsContents({
				seasonInfo: i_pageAnalysis.seriesInfo,
				qualifyingFastestLaps: i_pageAnalysis.qualifyingFastestLaps,
				raceFastestLaps: i_pageAnalysis.raceFastestLaps,
			}))
			break
		default: 
			setPageType('Page not recognized')
			break
	}
}

//const domContainer = document.querySelector('root')
//ReactDOM.render(React.createElement(ParsifalPopup), domContainer)

chrome.runtime.onMessage.addListener((request, sender) => {
	//document.getElementById('debug').innerText = JSON.stringify(request)
	switch (request.messageType) {
		case ContentScriptMessageTypes.pageAnalysisCompleted:
			handlePageAnalysisResults(request.pageType, { pageType: request.pageType, ...request })
			break
		case BackgroundServiceMessageTypes.pageAnalysisResultsResponse:
			handlePageAnalysisResults(request.pageType, request.pageAnalysis)
			break
	}
})

chrome.tabs.query({ active: true }, tabs => {
	sendMessage(PopupMessageTypes.pageAnalysisResultsRequest, {
		urlID: getSRSPageURLID(tabs[0].url),
	})
})
