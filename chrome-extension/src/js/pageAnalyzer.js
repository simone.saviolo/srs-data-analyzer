const PageTypes = {
	raceResults: 'raceResults',
	raceResultsList: 'raceResultsList',
	fastestLaps: 'fastestLaps',
}

const parseSRSTime = i_srsTimeString => {
	if (i_srsTimeString === '---' || i_srsTimeString === 'DNF') {
		return null;
	}
	if (i_srsTimeString === '') {
		return 0;
	}
	const regex = /((?<hours>\d?\d):)?(?<minutes>\d\d):(?<seconds>\d\d)\.(?<fractions>\d?\d?\d?)/
	const parseResult = regex.exec(i_srsTimeString)
	const time = {
		hours: parseInt(parseResult.groups.hours || '00'),
		minutes: parseInt(parseResult.groups.minutes || '00'),
		seconds: parseInt(parseResult.groups.seconds || '00'),
		milliseconds: parseInt((parseResult.groups.fractions || '000').padEnd(3, '0')),
	}
	return time.hours * 3600 + time.minutes * 60 + time.seconds + time.milliseconds / 1000.0
	/*return Duration.fromObject(time)*/
}

const parseRaceResults = (i_raceResultsTable) => {
	let drivers = []

	const body = i_raceResultsTable.querySelector('tbody')
	const allDriverRows = body.querySelectorAll('tr.tcat, tr.trow2, tr.content')
	const driverHeaderRows = body.querySelectorAll('tr.tcat, tr.trow2')
	const driverLapRows = body.querySelectorAll('tr.content')
	let lapRowsCounter = 0

	function buildDriverResultFromDriverRow(i_driverRow) {
		const cells = i_driverRow.querySelectorAll('td')
		
		const countryImg = cells.item(1).querySelector('img')
		const country = (countryImg && countryImg.getAttribute('src').slice(13, -4)) || ''
		
		const carBrandImg = cells.item(3).querySelector('img')
		const carBrand = (carBrandImg && carBrandImg.getAttribute('src').slice(27, -4)) || ''
		
		const driver = {
			position: parseInt(cells.item(0).innerText.slice(1)),
			country: country,
			name: cells.item(2).innerText,
			carName: cells.item(3).innerText,
			carBrand: carBrand,
			bestLapTime: parseSRSTime(cells.item(4).innerText),
			bestS1: parseSRSTime(cells.item(5).innerText),
			bestS2: parseSRSTime(cells.item(6).innerText),
			bestS3: parseSRSTime(cells.item(7).innerText),
			finishTime: parseSRSTime(cells.item(8).innerText),
			lapsNumber: parseInt(cells.item(9).innerText),
			laps: [],
			dnf: cells.item(8).innerText === 'DNF',
			incidents: parseInt(cells.item(10).innerText),
			points: parseInt(cells.item(11).innerText),
			totalPoints: parseInt(cells.item(12).innerText),
			domElement: i_driverRow,
		}

		return driver
	}

	function buildLapTimeFromLapRow(i_lapRow) {
		const lapCells = i_lapRow.querySelectorAll('td')
		
		return {
			lapNumber: parseInt(lapCells.item(9).innerText),
			position: parseInt(lapCells.item(0).innerText),
			lapTime: parseSRSTime(lapCells.item(4).innerText),
			s1Time: parseSRSTime(lapCells.item(5).innerText),
			s2Time: parseSRSTime(lapCells.item(6).innerText),
			s3Time: parseSRSTime(lapCells.item(7).innerText),
			domElement: i_lapRow,
		}
	}

	function isADriverRow(i_row) { return i_row.classList.contains('tcat') || i_row.classList.contains('trow2') }
	function isALapRow(i_row) { return i_row.classList.contains('content') }

	for (let i = 0; i < allDriverRows.length; ++i) {
		const dr = allDriverRows[i]

		if (isADriverRow(dr)) {
			const driver = buildDriverResultFromDriverRow(dr)

			let j = 0
			for (j = 1; j <= driver.lapsNumber && i + j < allDriverRows.length; ++j) {
				const lr = allDriverRows[i+j]
				if (isADriverRow(lr)) {
					break
				}

				const lap = buildLapTimeFromLapRow(lr)
				driver.laps.push(lap)
			}
			i += j - 1

			drivers.push(driver)
		}
	}
	
	return drivers
}
