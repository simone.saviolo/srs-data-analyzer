const colors = {
	fuchsia: '#ff8aff',
}

const arrayToN = N => Array.apply(null, {length: N}).map(Function.call, Number);

function groupBy(i_array, i_groupKeyFunction) {
	return i_array.reduce(function(groups, item) {
		const groupKey = i_groupKeyFunction(item)
		const groupIndex = groups.findIndex(g => g.key === groupKey)
		if (groupIndex >= 0) {
			groups[groupIndex].items.push(item)
		}
		else {
			groups.push({ key: groupKey, items: [ item ] })
		}
		return groups
	}, [])
}

const removeAllChildren = (i_element) => {
	while(i_element.firstChild) {
		i_element.removeChild(i_element.lastChild);
	}
}

const formatTime = (i_time) => {
	if (!i_time && i_time !== 0.0) {
		return '---'
	}

	const hours = i_time >= 3600 ? Math.floor(i_time / 3600).toLocaleString(undefined, { minimumIntegerDigits: 1, maximumFractionDigits: 0 }) : '';
	const minutes = Math.floor(i_time % 3600 / 60).toLocaleString(undefined, { minimumIntegerDigits: 2, maximumFractionDigits: 0 });
	const seconds = Math.floor(i_time % 60).toLocaleString(undefined, { minimumIntegerDigits: 2, maximumFractionDigits: 0 });
	const milliseconds = Math.round(i_time % 1 * 1000).toLocaleString(undefined, { minimumIntegerDigits: 3, maximumFractionDigits: 0 });
	return `${hours}${hours ? ':' : ''}${minutes}:${seconds}.${milliseconds}`
}

const formatGap = (i_gap, i_lapsGap) => {
	if (!i_gap && i_gap !== 0.0) {
		return '---'
	}

	if (i_lapsGap) {
		return `${i_lapsGap >= 0 ? '+' : '-'}${i_lapsGap} lap${Math.abs(i_lapsGap) > 1 ? 's' : ''}`
	}

	const hours = Math.abs(i_gap) >= 3600 ? Math.abs(Math.floor(i_gap / 3600)).toLocaleString(undefined, { minimumIntegerDigits: 1, maximumFractionDigits: 0 }) : '';
	const minutes = Math.abs(i_gap) >= 60 ? Math.abs(Math.floor(i_gap % 3600 / 60)).toLocaleString(undefined, { minimumIntegerDigits: hours ? 2 : 1, maximumFractionDigits: 0 }) : '';
	const seconds = Math.abs(Math.floor(i_gap % 60)).toLocaleString(undefined, { minimumIntegerDigits: minutes ? 2 : 1, maximumFractionDigits: 0 });
	const milliseconds = Math.abs(Math.round(i_gap % 1 * 1000)).toLocaleString(undefined, { minimumIntegerDigits: 3, maximumFractionDigits: 0 });
	return `${i_gap >= 0 ? '+' : '-'}${hours}${hours ? ':' : ''}${minutes}${minutes ? ':' : ''}${seconds}.${milliseconds}`
}
