const raceInfoContents = i_raceResults => {
	const $raceInfo = document.createElement('div')
	$raceInfo.classList.add('race-info')
	
	const $seriesInfo = document.createElement('div')
	$seriesInfo.classList.add('series-info')
	$raceInfo.appendChild($seriesInfo)
	
	const $seriesInfoHeader = document.createElement('header')
	$seriesInfoHeader.textContent = 'Series'
	$seriesInfo.appendChild($seriesInfoHeader)
	
	const $seriesInfoName = document.createElement('div')
	$seriesInfoName.classList.add('series-name')
	$seriesInfoName.textContent = i_raceResults.raceInfo.series.name
	$seriesInfo.appendChild($seriesInfoName)
	
	const $trackInfo = document.createElement('div')
	$trackInfo.classList.add('track-info')
	$raceInfo.appendChild($trackInfo)
	
	const $trackInfoHeader = document.createElement('header')
	$trackInfoHeader.textContent = 'Track'
	$trackInfo.appendChild($trackInfoHeader)
	
	const $trackInfoName = document.createElement('div')
	$trackInfoName.classList.add('track-name')
	$trackInfoName.textContent = i_raceResults.raceInfo.track.name
	$trackInfo.appendChild($trackInfoName)
	
	return $raceInfo
}

const driversResultsContents = i_raceResults => {
	const $ret = document.createElement('div')

	const winnerDriver = i_raceResults.drivers.find(d => d.position === 1)
	const allLaps = i_raceResults.drivers.reduce((flat, d) => [ ...flat, ...d.laps ], [])
	const allLapTimes = allLaps.map(l => l.lapTime).sort((a, b) => a - b)
	const minLapTime = allLapTimes.reduce((min, lt) => lt < min ? lt : min)
	const maxLapTime = allLapTimes.slice(0, Math.round(allLapTimes.length * 0.85)).reduce((max, lt) => lt > max ? lt : max)

	i_raceResults.drivers.map((driver, i, drivers) => {
		const $driver = document.createElement('div')
		$driver.classList.add('race-result-driver')
		$ret.appendChild($driver)
		
		const $position = document.createElement('div')
		$position.classList.add('race-result-driver__position')
		$position.textContent = driver.position
		$driver.appendChild($position)
		
		const $name = document.createElement('div')
		$name.classList.add('race-result-driver__name')
		$name.textContent = driver.name
		$driver.appendChild($name)

		const $charts = document.createElement('div')
		$charts.classList.add('race-result-driver__charts')
		$driver.appendChild($charts)
		
		const timeScatterChartData = {
			domain: { min: minLapTime, max: maxLapTime },
			values: driver.laps.map(l => l.lapTime < maxLapTime ? l.lapTime : maxLapTime),
		}
		const $lapsChart = document.createElement('canvas')
		$lapsChart.classList.add('race-result-driver__lap-times-chart')
		//$lapsChart.appendChild(timeScatterChart($lapsChart, timeScatterChartData))
		$charts.appendChild($lapsChart)
		timeScatterChart($lapsChart, timeScatterChartData)
		
		const $raceTime = document.createElement('div')
		$raceTime.classList.add('race-result-driver__race-time')
		$raceTime.textContent = driver.dnf
			? 'DNF'
			: driver.position === 1 
				? formatTime(driver.finishTime) 
				: formatGap(driver.finishTime - winnerDriver.finishTime, winnerDriver.lapsNumber - driver.lapsNumber)
		$driver.appendChild($raceTime)

	})

	return $ret
}

const resultsExportSection = (i_raceResults) => {
	const $ret = document.createElement('div')
	
	const [ $exportButton, $exportButtonLabel ] = exportToClipboardButton(
		'Export data to clipboard',
		undefined,
		() => {
			const startEvent = new CustomEvent('started', { detail: {} })
			$exportButton.dispatchEvent(startEvent)
			
			navigator.clipboard?.writeText(JSON.stringify(i_raceResults))

			const completedEvent = new CustomEvent('completed', { detail: { success: true } })
			$exportButton.dispatchEvent(completedEvent)
		}
	)
	$ret.appendChild($exportButton)
	
	return $ret
}

const raceResultsContents = i_raceResults => {
	const $ret = document.createElement('div')
	$ret.appendChild(resultsExportSection(i_raceResults))
	$ret.appendChild(raceInfoContents(i_raceResults))
	$ret.appendChild(driversResultsContents(i_raceResults))
	return $ret;
}