const DateTime = luxon.DateTime

const seasonInfoContents = i_seasonInfo => {
	const $raceInfo = document.createElement('div')
	$raceInfo.classList.add('season-info')
	
	const $seriesInfo = document.createElement('div')
	$seriesInfo.classList.add('series-info')
	$raceInfo.appendChild($seriesInfo)
	
	const $seriesInfoHeader = document.createElement('header')
	$seriesInfoHeader.textContent = 'Series'
	$seriesInfo.appendChild($seriesInfoHeader)
	
	const $seriesInfoName = document.createElement('div')
	$seriesInfoName.classList.add('series-name')
	$seriesInfoName.textContent = i_seasonInfo.series.name
	$seriesInfo.appendChild($seriesInfoName)
	
	const $simInfo = document.createElement('div')
	$simInfo.classList.add('sim-info')
	$raceInfo.appendChild($simInfo)
	
	const $simInfoHeader = document.createElement('header')
	$simInfoHeader.textContent = 'Sim'
	$simInfo.appendChild($simInfoHeader)
	
	const $simInfoName = document.createElement('div')
	$simInfoName.classList.add('sim-name')
	$simInfoName.textContent = i_seasonInfo.sim.name
	$simInfo.appendChild($simInfoName)
	
	return $raceInfo
}

const racesResultsContents = (i_seasonRaces, i_seasonInfo) => {
	const groupedRaces = groupBy(i_seasonRaces, race => race.track.name)

	const $ret = document.createElement('div')
	
	const [ $bulkLoadRacesButton, $bulkLoadRacesButtonLabel ] = exportToClipboardButton(
		`Load ${document.querySelectorAll('input[data-info="selected-for-bulk"][type="checkbox"]:checked').length} selected races`,
		undefined, 
		() => {
			const selectedRaces = Array.from(document.querySelectorAll('input[data-info="selected-for-bulk"][type="checkbox"]:checked'))
				.map(input => {
					const [ trackIndex, raceIndex ] = input.value.split('-')
					return groupedRaces[trackIndex].items[raceIndex]
				})
			bulkLoadRaces(selectedRaces, $bulkLoadRacesButton, i_seasonInfo)
		}
		)
	$ret.appendChild($bulkLoadRacesButton)

	const createRaceRow = (i_date, i_time, i_split, i_trackName, i_actionsCreator) => {
		const $race = document.createElement('div')
		$race.classList.add('season-result-race')
		
		const $date = document.createElement('div')
		$date.classList.add('season-result-race__date')
		$date.textContent = i_date
		$race.appendChild($date)
		
		const $time = document.createElement('div')
		$time.classList.add('season-result-race__time')
		$time.textContent = i_time
		$race.appendChild($time)
		
		const $split = document.createElement('div')
		$split.classList.add('season-result-race__split')
		$split.textContent = i_split
		$race.appendChild($split)
		
		const $track = document.createElement('div')
		$track.classList.add('season-result-race__track')
		$track.textContent = i_trackName
		$race.appendChild($track)
		
		const $actions = document.createElement('div')
		$actions.classList.add('season-result-race__actions')
		i_actionsCreator($actions)
		$race.appendChild($actions)
		
		return $race
	}

	groupedRaces.forEach((i_trackGroup, i) => {
		const $track = document.createElement('div')
		$track.classList.add('season-result-track')
		$ret.appendChild($track)

		const $trackBar = document.createElement('div')
		$trackBar.classList.add('season-result-track-bar')
		$trackBar.addEventListener('click', () => {
			$trackRaces.style.display = $trackRaces.style.display === 'none' ? 'block' : 'none'
		})
		$track.appendChild($trackBar)

		const $title = document.createElement('div')
		$title.classList.add('season-result-track__title')
		$trackBar.appendChild($title)
		
		const $trackName = document.createElement('div')
		$trackName.classList.add('season-result-track__name')
		$trackName.textContent = `#${i+1} - ${i_trackGroup.key}`
		$title.appendChild($trackName)
		
		const $trackRaceCount = document.createElement('div')
		$trackRaceCount.classList.add('season-result-track__race-count')
		$trackRaceCount.textContent = i_trackGroup.items.length + ' races'
		$title.appendChild($trackRaceCount)

		const $trackToggleAllRaces = document.createElement('button')
		$trackToggleAllRaces.classList.add('season-result-track__actions')
		$trackToggleAllRaces.textContent = 'Select/deselect all races'
		$trackToggleAllRaces.setAttribute('data-select', 'true')
		$trackToggleAllRaces.addEventListener('click', (e) => {
			const select = $trackToggleAllRaces.getAttribute('data-select') === 'true'
			$track.querySelectorAll('.season-result-track__races input[type="checkbox"][data-info="selected-for-bulk"]')
				.forEach(checkbox => checkbox.checked = select)
			$trackToggleAllRaces.setAttribute('data-select', !select)
			const count = document.querySelectorAll('input[data-info="selected-for-bulk"][type="checkbox"]:checked').length
			$bulkLoadRacesButtonLabel.textContent = `Load ${count} selected races`
			e.stopPropagation()
		})
		$trackBar.appendChild($trackToggleAllRaces)

		const $trackRaces = document.createElement('div')
		$trackRaces.classList.add('season-result-track__races')
		$track.appendChild($trackRaces)
		
		$trackRaces.appendChild(createRaceRow('Date (UTC)', 'Time (UTC)', 'Split', 'Track', ($i_actions) => { $i_actions.textContent = 'Actions' }))
		i_trackGroup.items.forEach((race, j) => {
			const $race = createRaceRow(
			race.dateTime.toUTC().toFormat('d MMM yyyy'),
			race.dateTime.toUTC().toFormat('HH:mm'),
			race.split,
			race.track.name,
			($actions) => {
				const $selectedForBulkLabel = document.createElement('label')
				
				{
					const $selectedForBulk = document.createElement('input')
					$selectedForBulk.setAttribute('type', 'checkbox')
					$selectedForBulk.setAttribute('data-info', 'selected-for-bulk')
					$selectedForBulk.setAttribute('value', i + '-' + j)
					$selectedForBulkLabel.appendChild($selectedForBulk)
				}
				
				$actions.appendChild($selectedForBulkLabel)
			}
			)
			$trackRaces.appendChild($race)
		})
	})

	$ret.querySelectorAll('input[data-info="selected-for-bulk"][type="checkbox"]').forEach($checkbox => {
		$checkbox.addEventListener('change', () => {
			const count = document.querySelectorAll('input[data-info="selected-for-bulk"][type="checkbox"]:checked').length
			$bulkLoadRacesButtonLabel.textContent = `Load ${count} selected races`
		})
	})

	return $ret
}

const seasonResultsContents = i_seasonResults => {
	const $ret = document.createElement('div')
	$ret.appendChild(seasonInfoContents(i_seasonResults.seasonInfo))
	$ret.appendChild(racesResultsContents(i_seasonResults.racesList, i_seasonResults.seasonInfo))
	return $ret;
}

const bulkLoadRaces = (i_races, $i_button, i_seasonInfo) => {
	let raceResultsList = []
	let fetchPromises = []
	i_races.forEach(race => {
		const srsPageURL = race.raceResultsURL[0] === '/' 
			? 'https://www.simracingsystem.com' + race.raceResultsURL
			: race.raceResultsURL
		fetchPromises.push(new Promise((resolve, reject) => {
			fetch(srsPageURL)
			.then(response => response.text(), r => reject(r))
			.then(t => {
				try {
					const parser = new DOMParser()
					const raceResultsDocument = parser.parseFromString(t, 'text/html')
					let raceResults = {
						dateTime: race.dateTime,
						split: race.split,
						track: race.track,
					}
					raceResultsDocument.querySelectorAll('table').forEach(table => {
						if (table.classList.contains('tborder')) {
							const header = table.querySelector('thead');
							if (header) {
								raceResults.drivers = parseRaceResults(table)
							}
						}
					})
					raceResultsList.push(raceResults)
					resolve(raceResults)
				}
				catch (e) {
					reject(e)
				}
			}, 
			r => reject(r))
		}))
	})

	const event = new CustomEvent('started', {
		detail: {
			total: fetchPromises.length,
		},
	})
	$i_button.dispatchEvent(event)

	let completedPromisesCount = 0
	fetchPromises.forEach(p => {
		p.then(rr => {
			const event = new CustomEvent('progress', {
				detail: {
					completed: ++completedPromisesCount,
					total: fetchPromises.length,
				},
			})
			$i_button.dispatchEvent(event)
		})
	})

	Promise.all(fetchPromises)
	.then(() => {
		const event = new CustomEvent('completed', {
			detail: {
				success: true,
				completed: completedPromisesCount,
				total: fetchPromises.length,
		}
		})
		$i_button.dispatchEvent(event)

		const result = {
			seasonInfo: i_seasonInfo,
			raceResultsList: raceResultsList,
		}
		sendMessage(PopupMessageTypes.broadcastSeasonRaceResults, result)
		navigator.clipboard.writeText(JSON.stringify(result))
	},
	r => {
		const event = new CustomEvent('completed', {
			detail: {
				success: false,
				completed: completedPromisesCount,
				total: fetchPromises.length,
				error: r,
			}
		})
		$i_button.dispatchEvent(event)
	})
}
