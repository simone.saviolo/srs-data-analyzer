function timeScatterChart($i_root, i_data) {
	//const ctx = $i_root.getContext('2d');
	const data = {
		//labels: arrayToN(i_data.values.length).map(x => x + 1),
		label: 'Lap times',
		datasets: [{
			data: i_data.values.map(v => ({ x: v, y: 0 })),
			fill: true,
			pointStyle: 'line',
			rotation: 90,
			radius: 5,
			borderColor: '#f00',
			backgroundColor: '#c00',
		}],
	}
	const chart = new Chart($i_root, {
		type: 'scatter',
		data: data,
		options: {
			responsive: true,
			maintainAspectRatio: false,
			plugins: {
				legend: {
					display: false,
				},
				tooltip: {
					enabled: false,
				},
			},
			scales: {
				x: {
					display: false,
					min: i_data.domain.min,
					max: i_data.domain.max,
					grace: 0,
					reverse: true,
				},
				y: {
					display: false,
					min: -0.1, 
					max: 0.1,
					grace: 0,
				},
			},
		},
	})

	/*const lineWidth = 0.01
	const data = {
		labels: [''],
		datasets: i_data.values.map(v => ({
			data: [ [ v - lineWidth/2, v + lineWidth/2 ] ],
			backgroundColor: '#f00',
		}))
	}
	console.log(data)
	const chart = new Chart($i_root, {
		type: 'bar',
		data: data,
		options: {
		  indexAxis: 'y',
		  //maintainAspectRatio: false,
		  responsive: true,
		  plugins: {
			legend: {
			  display: false
			},
			tooltip: {
			  callbacks: {
				label: ctx => data[ctx.datasetIndex]
			  }
			}
		  },
		  interaction: {
			mode: 'x',
		  },
		  scales: {
			y: {
			  stacked: true
			},
			x: {
			  max: i_data.domain.max,
			  min: i_data.domain.min,
			  reverse: true
			}
		  }
		}
	  })*/

	return chart
}
