// const seasonInfoContents // already defined

const fastestLapsEventLeaderboardContents = (i_fastestLaps, i_isQualifying) => {
	const $eventLeaderboardContainer = document.createElement('div')
	$eventLeaderboardContainer.classList.add('event-leaderboard')
	
	const $title = document.createElement('h3')
	$title.textContent = i_isQualifying ? 'Qualifying best laps' : 'Race best laps'
	$eventLeaderboardContainer.appendChild($title)
	
	const topFastestLap = i_fastestLaps.fastestLaps.find(fl => fl.position === 1)
	i_fastestLaps.fastestLaps.forEach((fastestLap) => {
		const $row = document.createElement('div')
		$row.classList.add('fastest-lap')
		$eventLeaderboardContainer.appendChild($row)
		
		const $position = document.createElement('div')
		$position.classList.add('fastest-lap__position')
		$position.textContent = fastestLap.position
		$row.appendChild($position)
		
		const $driverName = document.createElement('div')
		$driverName.classList.add('fastest-lap__driver-name')
		$driverName.textContent = fastestLap.driverName
		$row.appendChild($driverName)
		
		const $time = document.createElement('div')
		$time.classList.add('fastest-lap__time')
		$time.textContent = fastestLap.position === 1 ? formatTime(fastestLap.lapTime) : formatGap(fastestLap.lapTime - topFastestLap.lapTime)
		$row.appendChild($time)
	})	
	
	return $eventLeaderboardContainer
}	

const fastestLapsEventContents = (i_data) => {
	const $ret = document.createElement('div')
	$ret.classList.add('event-container')
	$ret.setAttribute('data-event-code', i_data.eventCode)

	$ret.appendChild(fastestLapsEventLeaderboardContents(i_data.qualifyingFastestLaps, true))
	$ret.appendChild(fastestLapsEventLeaderboardContents(i_data.raceFastestLaps, false))
	
	return $ret
}

const fastestLapsContents = i_data => {
	const $ret = document.createElement('div')
	$ret.appendChild(seasonInfoContents(i_data.seasonInfo))

	const $actionBar = document.createElement('div')
	$ret.appendChild($actionBar)

	const [ $exportButton, $exportButtonLabel ] = exportToClipboardButton(
		'Export data to clipboard',
		undefined,
		() => {
			const startEvent = new CustomEvent('started', { detail: {} })
			$exportButton.dispatchEvent(startEvent)
			
			navigator.clipboard?.writeText(JSON.stringify(i_data))

			const completedEvent = new CustomEvent('completed', { detail: { success: true } })
			$exportButton.dispatchEvent(completedEvent)
		}
		)
	$actionBar.appendChild($exportButton)
	
	i_data.qualifyingFastestLaps.map(fl => fl.event.code).forEach(eventCode => {
		const $eventButton = document.createElement('button')
		$eventButton.textContent = eventCode
		$eventButton.addEventListener('click', () => {
			const $container = document.querySelectorAll('.event-container').forEach($ec => {
				$ec.style.display = $ec.getAttribute('data-event-code') === eventCode ? 'block' : 'none'
			})
		})
		$ret.appendChild($eventButton)
	})

	i_data.qualifyingFastestLaps.map(fl => fl.event.code).forEach(eventCode => {
		const $eventContainer = fastestLapsEventContents({
			eventCode: eventCode,
			qualifyingFastestLaps: i_data.qualifyingFastestLaps.find(fl => fl.event.code === eventCode),
			raceFastestLaps: i_data.raceFastestLaps.find(fl => fl.event.code === eventCode),
		})
		$ret.appendChild($eventContainer)
	})
	return $ret;
}
