function exportToClipboardButton(i_labelText, i_classes, i_clickCallback) {
	const $retval = document.createElement('button')
	$retval.classList.add([ 'export-to-clipboard-btn', ...(i_classes || []) ])
	
	const $retvalLabel = document.createElement('span')
	$retvalLabel.textContent = i_labelText
	$retval.appendChild($retvalLabel)

	const $successPopup = document.createElement('div')
	$successPopup.classList.add('export-to-clipboard-btn__success-popup')
	$successPopup.innerText = 'Copied to the clipboard!'
	//$retval.appendChild($successPopup)

	const $progressBar = document.createElement('div')
	$progressBar.classList.add('export-to-clipboard-btn__progress-bar')
	$retval.appendChild($progressBar)

	const $progressBarProgress = document.createElement('div')
	$progressBarProgress.classList.add('progress')
	$progressBar.appendChild($progressBarProgress)

	const $progressBarLabel = document.createElement('div')
	$progressBarLabel.classList.add('label')
	$progressBar.appendChild($progressBarLabel)

	$retval.addEventListener('click', () => {
		i_clickCallback()
		/*$successPopup.classList.add('visible')
		window.setTimeout(() => {
			$successPopup.classList.remove('visible')
		}, 5000)*/
	})

	let listenToProgress = false

	function updateProgress(i_progress, i_label = null) {
		if (listenToProgress) {
			$progressBarProgress.style.width = i_progress + '%'
			$progressBarLabel.textContent = i_label || (i_progress + '%')
		}
	}

	$retval.addEventListener('started', e => {
		listenToProgress = true
		const progress = 0
		updateProgress(progress)
	})
	
	$retval.addEventListener('progress', e => {
		const progress = e.detail.completed * 100.0 / e.detail.total
		updateProgress(progress)
	})
	
	$retval.addEventListener('completed', e => {
		const progress = e.detail.completed * 100.0 / e.detail.total
		updateProgress(progress, e.detail.success ? 'Ok' : 'Error')
		listenToProgress = false
	})

	return [ $retval, $retvalLabel ]
}
