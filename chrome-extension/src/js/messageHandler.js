const ContentScriptMessageTypes = {
	pageAnalysisCompleted: 'pageAnalysisCompleted',
	raceResultsListCompleted: 'raceResultsListCompleted',
}

const PopupMessageTypes = {
	pageAnalysisResultsRequest: 'pageAnalysisResultsRequest',
	broadcastSeasonRaceResults: 'broadcastSeasonRaceResults',
	broadcastSeasonRaceResultsProgress: 'broadcastSeasonRaceResultsProgress',
}

const BackgroundServiceMessageTypes = {
	pageAnalysisResultsResponse: 'pageAnalysisResultsResponse',
}

const sendMessage = (i_messageType, i_payload) => {
	chrome.runtime.sendMessage({
		messageType: i_messageType,
		...i_payload,
	})
}

const getSRSPageURLID = i_url => {
	const regexResult = /\/(?<urlid>raceresultssrs.php?.+)&e=.*/.exec(i_url)
	if (!regexResult || regexResult.length === 0) {
		return null
	}
	return regexResult.groups.urlid
}
