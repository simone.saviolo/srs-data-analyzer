chrome.runtime.onInstalled.addListener(() => {
	chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
		chrome.declarativeContent.onPageChanged.addRules([{
			conditions: [
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: {
						hostContains: 'simracingsystem.com'
					},
				}),
			],
			actions: [
				new chrome.declarativeContent.ShowPageAction(),
			]
		}]);
	});
});

let pageAnalyses = {}

chrome.runtime.onMessage.addListener((request, sender) => {
	switch (request.messageType) {
		
		case ContentScriptMessageTypes.pageAnalysisCompleted:
			const urlID = getSRSPageURLID(sender.url)
			pageAnalyses[urlID] = request
			break

		case PopupMessageTypes.pageAnalysisResultsRequest:
			const pageAnalysis = pageAnalyses[request.urlID]
			sendMessage(BackgroundServiceMessageTypes.pageAnalysisResultsResponse, {
				pageAnalysis: pageAnalysis
			})
			break

	}
})
