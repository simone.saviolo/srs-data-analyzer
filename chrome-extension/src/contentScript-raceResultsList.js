const DateTime = luxon.DateTime

const data = {
	seasonInfo: null,
	racesList: null,
}
let raceResultsList = []

const actOnRaceResultsListTable = ($i_table) => {
	let races = []
	$i_table.querySelectorAll('tbody > tr').forEach($tr => {
		const cells = $tr.querySelectorAll('td')

		const dateTimeParseResult = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2}) (?<hour>\d{2}):(?<minute>\d{2})/.exec(cells.item(0).innerText)
		const dateTime = DateTime.fromObject({
			years: dateTimeParseResult.groups.year,
			months: dateTimeParseResult.groups.month,
			days: dateTimeParseResult.groups.day,
			hours: dateTimeParseResult.groups.hour,
			minutes: dateTimeParseResult.groups.minute,
		})
		const trackName = cells.item(2).innerText
		const $a = cells.item(4).querySelector('a')
		const $actualLink = $a.querySelector('span')
		const raceResultsURL = /window\.open\('\.(?<url>\/.+)','_self'/.exec($actualLink.getAttribute('onclick'))
			.groups.url

		races.push({
			dateTime: dateTime,
			split: parseInt(cells.item(1).innerText),
			track: {
				name: trackName,
			},
			domElement: $tr,
			linkToRaceResults: $a,
			raceResultsURL: raceResultsURL,
		})
	})
	return races.reverse()
}

const actOnSeriesInfoTable = (i_table) => {
	const cell = i_table.querySelector('tbody tr:first-child > td')
	if (!cell) return;

	const imgs = cell.querySelectorAll('img')
	const seriesCode = /serieslogo\/(?<seriesCode>.+)\.png/.exec(imgs.item(0).getAttribute('src')).groups.seriesCode
	const simCode = /results\/(?<simCode>.+)\.png/.exec(imgs.item(1).getAttribute('src')).groups.simCode
	
	return {
		series: {
			code: seriesCode,
			name: seriesCode,
		},
		sim: {
			code: simCode,
			name: simCode,
		},
	}
}

document.querySelectorAll('table').forEach(table => {
	if (table.classList.contains('tborder')) {
		const parent = table.parentElement
		if (parent.nodeName === 'DIV' && parent.id === 'content') {
			data.seasonInfo = actOnSeriesInfoTable(table)
		}
		else {
			data.racesList = actOnRaceResultsListTable(table)
		}
	}
})

sendMessage(ContentScriptMessageTypes.pageAnalysisCompleted, {
	pageType: PageTypes.raceResultsList,
	seriesInfo: data.seasonInfo,
	racesList: data.racesList,
})
