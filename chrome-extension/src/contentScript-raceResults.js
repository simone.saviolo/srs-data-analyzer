let raceResults = {
	raceInfo: undefined,
	drivers: undefined,
}

const actOnRaceResultsTable = (table) => {
	raceResults.drivers = parseRaceResults(table)

	sendMessage(ContentScriptMessageTypes.pageAnalysisCompleted, {
		pageType: PageTypes.raceResults,
		raceResults: raceResults,
	})

	const markAsBestTiming = el => {
		el.classList.add('parsifal_best-timing')
		//el.style.backgroundColor = colors.fuchsia
		//el.style.fontWeight = 'bold'
	}
	const markAsDriverBestTiming = el => {
		el.classList.add('parsifal_driver-best-timing')
		//el.style.backgroundColor = colors.fuchsia
		//el.style.fontWeight = 'bold'
	}
	const markAsBestLapOwner = el => {
		el.classList.add('parsifal_best-lap-owner')
		//el.style.color = colors.fuchsia
		//el.style.fontWeight = 'bold'
	}
	const markAsBestSectorOwner = el => {
		el.classList.add('parsifal_best-sector-owner')
		//el.style.borderBottomColor = colors.fuchsia
		//el.style.borderBottomWidth = '4px'
		//el.style.borderBottomStyle = 'solid'
	}
	const addBestTimingBadge = el => {
		const bestTimingBadge = document.createElement('div')
		bestTimingBadge.classList.add('parsifal_best-timing-icon')
		el.classList.add('parsifal_badge-container')
		el.appendChild(bestTimingBadge)
	}

	// Reset all styles
	raceResults.drivers.forEach((driver, driverIndex) => {
		driver.laps.forEach((lap, lapIndex) => {
			lap.domElement.style.fontWeight = 'normal'
			lap.domElement.style.color = '#fff'
		})
	})

	// Drivers' best performance
	raceResults.drivers.forEach((driver, driverIndex) => { 
		if (driver.laps.length === 0) {
			return
		}
		
		// Drivers' best lap
		const { bestLap, bestLapIndex } = driver.laps
		.map((lap, lapIndex) => ({ bestLap: lap.lapTime, bestLapIndex: lapIndex }))
		.reduce((min, lap) => (lap.bestLap > 0 && lap.bestLap < min.bestLap) ? lap : min)
		const driverBestLapDriverCells = raceResults.drivers[driverIndex].laps[bestLapIndex].domElement.querySelectorAll('td');
		markAsDriverBestTiming(driverBestLapDriverCells.item(4));

		// Drivers' best sectors
		const highlightBestSector = (i_sectorSelector, i_cellIndex) => {
			const bestSector = driver.laps
				.reduce((acc, lap, lapIndex) => [ ...acc, { t: i_sectorSelector(lap), i: lapIndex, el: lap.domElement, parentEl: driver.domElement } ], [])
				.reduce((min, lap) => (lap.t > 0 && lap.t < min.t) ? lap : min)
			if (bestSector.t === 0) {
				return
			}
			const sectorCell = bestSector.el.querySelectorAll('td').item(i_cellIndex)
			markAsDriverBestTiming(sectorCell)
		}
		highlightBestSector(lap => lap.s1Time, 5)
		highlightBestSector(lap => lap.s2Time, 6)
		highlightBestSector(lap => lap.s3Time, 7)
	})
	
	// Overall best lap
	const { bestLap, bestLapIndex } = raceResults.drivers
	.map((dr, i) => ({ bestLap: dr.bestLapTime, bestLapIndex: i }))
	.reduce((min, dr) => (dr.bestLap > 0 && dr.bestLap < min.bestLap) ? dr : min)
	const bestLapDriverCells = raceResults.drivers[bestLapIndex].domElement.querySelectorAll('td');
	markAsBestTiming(bestLapDriverCells.item(4));
	[ 0, 1, 2, 3 ].map(x => bestLapDriverCells.item(x)).forEach(markAsBestLapOwner)
	markAsBestLapOwner(bestLapDriverCells.item(2).querySelector('a'))
	addBestTimingBadge(bestLapDriverCells.item(2))
	
	// Overall best sectors
	const highlightBestSector = (i_sectorSelector, i_cellIndex) => {
		const bestSector = raceResults.drivers
			.reduce((acc, d, i) => [ ...acc, ...d.laps.map(l => ({ t: i_sectorSelector(l), i: i, el: l.domElement, parentEl: d.domElement })) ], [])
			.reduce((min, l) => (l.t > 0 && l.t < min.t) ? l : min)
		if (bestSector.t === 0) {
			return
		}
		const sectorCell = bestSector.el.querySelectorAll('td').item(i_cellIndex)
		markAsBestTiming(sectorCell)
		//addBestTimingBadge(sectorCell)
		markAsBestSectorOwner(bestSector.parentEl.querySelectorAll('td').item(i_cellIndex))
	}
	highlightBestSector(lap => lap.s1Time, 5)
	highlightBestSector(lap => lap.s2Time, 6)
	highlightBestSector(lap => lap.s3Time, 7)
}

const actOnRaceInfoTable = (table) => {
	const cell = table.querySelector('tr:first-child > td');
	if (!cell) return;

	const imgs = cell.querySelectorAll('img')
	const seriesCode = /serieslogo\/(?<seriesCode>.+)\.png/.exec(imgs.item(0).getAttribute('src')).groups.seriesCode
	const trackCode = /tracklogo\/.+\/(?<trackCode>.+)\.png/.exec(imgs.item(1).getAttribute('src')).groups.trackCode
	
	raceResults.raceInfo = {
		series: {
			code: seriesCode,
			name: seriesCode,
		},
		track: {
			code: trackCode,
			name: trackCode,
		},
	}
}

document.querySelectorAll('table').forEach(table => {
	if (table.classList.contains('tborder')) {
		const header = table.querySelector('thead');
		if (header) {
			actOnRaceResultsTable(table)
		}
		else {
			actOnRaceInfoTable(table)
		}
	}
})
